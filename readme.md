## Requisitos
PHP >= 7.1.3
BCMath PHP Extension
Ctype PHP Extension
JSON PHP Extension
Mbstring PHP Extension
OpenSSL PHP Extension
PDO PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Composer
Node 8.12 >=
## Instrucciones
-Crear una BD en MariaDB llamada library con un usuario llamado root sin contraseña(solo para pruebas :V)
-En la linea de comandos en la raiz del proyecto ejecutar el comando: composer update
-En la linea de comandos en la raiz del proyecto ejecutar el comando: php artisan migrate
-En la linea de comandos en la raiz del proyecto ejecutar el comando: npm install
-En la linea de comandos en la raiz del proyecto ejecutar el comando: npm run dev
-En la linea de comandos en la raiz del proyecto ejecutar el comando:php artisan db:seed
## Acceso
Email: lambetdelta@gmail.com
Contraseña: 11111111
